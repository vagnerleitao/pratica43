
import utfpr.ct.dainf.if62c.pratica.FiguraComLados;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;
import utfpr.ct.dainf.if62c.pratica.Quadrado;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica43 {
    //private static double valorArea;
    //private static double valorPerimetro;
    //private static double ladom;
    //private static double ladoM;
    
    public static void main(String[] args) {
        String areaq = "O valor da area do quadrado e : ";
        String perq = "\nO valor do Perimetro do quadrado e : ";
        String arear = "O valor da area do retangulo e : ";
        String perr = "\nO valor do Perimetro do retangulo e : ";
        String areate = "O valor da area do triangulo equilatero e : ";
        String perte = "\nO valor do Perimetro do triangulo equilatero e : ";

        FiguraComLados[] figuras=new FiguraComLados[3];
        
        figuras[0] = new Quadrado(10);
        figuras[1] = new TrianguloEquilatero(5.264);
        figuras[2] = new Retangulo(2,5);
        
/*        Quadrado figq= new Quadrado();
        valorArea = figq.getArea();
        valorPerimetro = figq.getPerimetro(); */
        System.out.println(areaq+figuras[0].getArea()+perq+figuras[0].getPerimetro());
        System.out.println(areate+figuras[1].getArea()+perte+figuras[1].getPerimetro());
        System.out.println(arear+figuras[2].getArea()+perr+figuras[2].getPerimetro());
/*        eixom=fig.getEixoMenor();
        eixoM=fig.getEixoMaior();
        Elipse fig1 = new Elipse(eixom,eixoM);
        valorArea=fig1.getArea();
        valorPerimetro=fig1.getPerimetro();
        System.out.println(areae+valorArea+pere+valorPerimetro+"\n");

        Circulo fig2 = new Circulo(1);
        eixom=fig2.getEixoMenor();
        //eixoM=fig2.getEixoMaior();
        Circulo fig3 = new Circulo(eixom);
        valorArea=fig3.getArea();
        valorPerimetro=fig3.getPerimetro();
        System.out.println(areac+valorArea+perc+valorPerimetro+"\n"); */
    }
}
