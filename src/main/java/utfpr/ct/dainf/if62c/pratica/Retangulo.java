package utfpr.ct.dainf.if62c.pratica;


import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vagner
 */
public class Retangulo implements FiguraComLados, Serializable{
    private double ladom;
    private double ladoM;
    private double area;
    private double perimetro;
    
    public Retangulo() {
        super();
    }
    
    public Retangulo(double ladom, double ladoM) {
        this.ladom=ladom;
        this.ladoM=ladoM;
    }
    
    @Override
    public double getLadoMenor() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return ladom;
    }

    @Override
    public double getLadoMaior() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return ladoM;
    }

    @Override
    public String getNome() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getPerimetro() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        perimetro=2*ladom+2*ladoM;
        return perimetro;
    }

    @Override
    public double getArea() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        area=ladom*ladoM;
        return area;
    }
    
}
