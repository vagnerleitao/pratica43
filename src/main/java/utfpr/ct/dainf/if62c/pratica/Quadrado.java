package utfpr.ct.dainf.if62c.pratica;


//import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vagner
 */
//public class Quadrado implements FiguraComLados, Serializable{
public class Quadrado extends Retangulo{
    private double lado;
    private double area;
    private double perimetro;
    
    public Quadrado() {
        super();
    }
    
    public Quadrado(double lado) {
        this.lado=lado;
    }
    
    @Override
    public double getLadoMenor() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return lado;
    }

    @Override
    public double getLadoMaior() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return lado;
    }

    @Override
    public String getNome() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getPerimetro() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        perimetro = 4*this.getLadoMenor();
        return perimetro;
    }
   

    @Override
    public double getArea() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        area = this.getLadoMenor()*this.getLadoMenor();
        return area;
    }
}
